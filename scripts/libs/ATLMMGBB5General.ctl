void turnOnOff()
{
 
        string buttonName;
        getValue("","name",buttonName);
        DebugN("THIS IS A SCRIPT IN THE ATLMMGBB5CNTRL LIB TO TURN ON/OFF SECTIONS",buttonName," of ", $dpChamber);

        dyn_string dynNames, layerPS_Chan;
        string sectNames=$dpChamber+".CH-HVchan";
        dpGet( sectNames, dynNames);
        string dpLayChan=$dpChamber+".PS-HVchan.L"+$Layer;
        dpGet( dpLayChan, layerPS_Chan);
 
         int ii=0;
         for (int i=1; i<=dynlen(dynNames); i++) 
           {
             DebugN("substr(buttonName) == dynNames[i]:",substr(buttonName,7,2), dynNames[i],substr(dynNames[i],0,2));
             if (substr(buttonName,7,2) == substr(dynNames[i],0,2)) 
               { ii=i; 
                 DebugN(" BUTTON index: ",ii);
                 break; }
           }

         string Col;
         getValue("","backCol",Col);
         bool newstate;
         if (Col==GColOff) 
         {
           newstate=true;           
           Col==GColOn;
         }
         else 
           {     
             newstate=FALSE;      
             Col==GColOff;
           }

         if (ii==dynlen(dynNames)) ii=dynlen(layerPS_Chan);
         dpSet(layerPS_Chan[ii]+".settings.onOff",newstate); 

}

void storeData(string dpchamber)
{
         DebugN(" storeData:",dpchamber);
  
         string sQuadID,sQuadtype,sQuadsite;
         int    QuadPos;
         
         dpGet( $dpChamber + ".ProductionNumber:",  sQuadID);
         dpGet( $dpChamber + ".Type:",              sQuadtype);
         dpGet( $dpChamber + ".ProductionSite:",    sQuadsite);
         dpGet( $dpChamber + ".ChamberNum:",        QuadPos);

         string     filepath;
         filepath=path+"ChamberID-"+sQuadID+"_Typ-"+sQuadtype+"_Pos-"+(string) QuadPos+"_data.txt";
         file fopenreturn=fopen(filepath,"w+");
         DebugN("FILE ",filepath, " was created, return ",fopenreturn);  
  
         string sChnum=substr($dpChamber,14,1);  

         float BB5Gas,BB5ambPr,BB5Temp,CSGas,CSambPr,CSTemp;
         
         dpGet( $dpChamber + ".Gas:",      BB5Gas);
         dpGet( $dpChamber + ".ambPr:",    BB5ambPr);
         dpGet( $dpChamber + ".Temp:",     BB5Temp);
         
         dpGet( $dpChamber + ".CS-Gas:",   CSGas);
         dpGet( $dpChamber + ".CS-ambPr:", CSambPr);
         dpGet( $dpChamber + ".CS-Temp:",  CSTemp);

         DebugN("sQuadID:",sQuadID," sQuadtype:",sQuadtype," sQuadsite:",sQuadsite," QuadPos:",QuadPos);
         DebugN(" BB5Gas:",BB5Gas, " BB5ambPr:", BB5ambPr, " BB5Temp:",  BB5Temp);
         DebugN(" CSGas:", CSGas,  " CSambPr:",  CSambPr,  " CSTemp:",   CSTemp);
                       
         fprintf(fopenreturn," QuadID %15s QuadType %10s CS %15s QuadPosition %3d %s",sQuadID,sQuadtype,sQuadsite,QuadPos,"\n");
         fprintf(fopenreturn," BB5Gas %5.2f BB5ambPr %5.1f  BB5Temp %5.1f  %s",  BB5Gas,BB5ambPr,BB5Temp,"\n");
         fprintf(fopenreturn,"  CSGas %5.2f  CSambPr %5.1f   CSTemp %5.1f  %s",   CSGas, CSambPr, CSTemp,"\n");

//         fputs(" UuasID "+sQuadID    +"\n",fopenreturn);
//         fputs(sQuadtype  +"\n",fopenreturn);
//         fputs(sQuadsite  +"\n",fopenreturn);
//         fputs((string) QuadPos   +"\n",fopenreturn);

         
//         fputs(setstrmaxHV+"\n",fopenreturn);
//         fputs(setdrimaxHV+"\n",fopenreturn);

//       DebugN("FILE ",filepath, " write return ",fopenreturn);  

         dyn_int    HVoper,HVmax,HVexc,CSconformFL,conformFL;
         dyn_float  Imax;
         dyn_float  CSImax,CSHVmax,CSHVoper;
         dyn_string HVrem,sectName;

         dpGet( $dpChamber + ".CH-HVchan:", sectName);

         fprintf(fopenreturn,"  %120s %s","Layer-sectNum-sectName--BB5HVmax--BB5HVop--BB5conFL--BB5Imax"+
                                          "------CSHVmax--CSHVoper--CSconFL-CSImax--------"
                                          +"Exception---------------Remarks--","\n");
                

         for (int i=1; i<=4;i++)
          {        
           dpGet( $dpChamber + ".maxHV.L"      +(string)i+":", HVmax);
           dpGet( $dpChamber + ".operHV.L"     +(string)i+":", HVoper);
           dpGet( $dpChamber + ".maxI.L"       +(string)i+":", Imax);
           dpGet( $dpChamber + ".confFL.L"     +(string)i+":", conformFL);
           
           dpGet( $dpChamber + ".CS-maxHV.L"   +(string)i+":", CSHVmax);
           dpGet( $dpChamber + ".CS-operHV.L"  +(string)i+":", CSHVoper);
           dpGet( $dpChamber + ".CS-maxI.L"    +(string)i+":", CSImax); 
           dpGet( $dpChamber + ".CS-confFL.L"  +(string)i+":", CSconformFL);
           
           dpGet( $dpChamber + ".ExceptionHV.L"+(string)i+":", HVexc); 
           dpGet( $dpChamber + ".Notes.L"      +(string)i+":", HVrem);

           DebugN("Store data CSHVmax:",CSHVmax);

           for (int j=1;j<=LminNumRows[(int)sChnum][i];j++) 
             {
               if (dynlen(conformFL)  == j-1) dynAppend(conformFL,  -2);
               if (dynlen(Imax)       == j-1) dynAppend(Imax,       -2);
               if (dynlen(CSconformFL)== j-1) dynAppend(CSconformFL,-2);
               if (dynlen(CSImax)     == j-1) dynAppend(CSImax,     -2);
             
               fprintf(fopenreturn," %3d %3d %10s    %8d %8d %8d %8.4f    %8.1f %8.1f %8d %8.4f   %8d   %30s %s",i,j,sectName[j],  
                     HVmax[j],  HVoper[j],  conformFL[j],  Imax[j],
                   CSHVmax[j],CSHVoper[j],CSconformFL[j],CSImax[j],HVexc[j],HVrem[j],"\n");

               DebugN("Store data fprintf CSHVmax:",CSHVmax[j]);
             }                    // jloop      

//           DebugN(" Layer:",i," SectionName:",sectName,
//                  " HVmax:",   HVmax,   " HVoper:",HVoper," Imax:",       Imax,       " conformFL:",conformFL," CSHVmax:",CSHVmax,
//                  " CSHVoper:",CSHVoper," CSImax:",CSImax," CSconformFL:",CSconformFL," HVexc:",    HVexc);
          }                       //Layer 
         
        fclose(fopenreturn);   

}

void readExistingFile(string dpChamber,string filepath)
{
          string sChnum=substr($dpChamber,14,1); 
          
          DebugN(" readExistingFile data for Chamber:",dpChamber,$dpChamber,sChnum);
          DebugN(" ReadExistingFile Found file:",filepath, " READ FROM FILE");
          DebugN("ReadExistingFile  LminNumRows:",LminNumRows);

          string s;
          int size=getFileSize(filepath);
          DebugN("File size : ",size);
          file fopenreturn=fopen(filepath,"r");
          DebugN(" fopen Return :",fopenreturn);

          string s1,sQuadID,s2,sQuadtype,s3,sQuadsite,s4,s5,s6,s7,s8,s9,s10,s11,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23;
          int    QuadPos;
          float BB5Gas,BB5ambPr,BB5Temp,CSGas,CSambPr,CSTemp;
          dyn_int    HVoper,HVmax,HVexc,dynIntempty,conformFL,CSconformFL;
          dyn_float  Imax,CSImax,CSHVmax,CSHVoper;
          dyn_string HVrem,dynStrempty;

          
          rewind(fopenreturn);
          fscanf(fopenreturn," %s %15s %s %10s %s %15s %s %3d",s1,sQuadID,s2,sQuadtype,s3,sQuadsite,s4,QuadPos);
          fscanf(fopenreturn," %s %f  %s %f  %s %f ", s5, BB5Gas,s6,BB5ambPr,s7, BB5Temp);
          fscanf(fopenreturn," %s %f  %s %f  %s %f ", s8,  CSGas,s9, CSambPr,s10, CSTemp);
//          fscanf(fopenreturn," %s  %s %s %s %s %s %s %s %s %s %s %s %s", s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23);
          fscanf(fopenreturn," %s",s11);
          DebugN( s11);

//          fscanf(fopenresult,"%s",sQuadID);
//          fscanf(fopenresult,"%s",sQuadtype);
//          fscanf(fopenresult,"%s",sQuadsite);
//          fscanf(fopenresult,"%s",setstrmaxHV);
//          fscanf(fopenresult,"%s",setdrimaxHV);

          DebugN( s1,  sQuadID);
          DebugN( s2,  sQuadtype);
          DebugN( s3,  sQuadsite);
          DebugN( s4,  QuadPos);
          DebugN( s5,  BB5Gas);
          DebugN( s6,  BB5ambPr);
          DebugN( s7,  BB5Temp);
          DebugN( s8,  CSGas);
          DebugN( s9,  CSambPr);
          DebugN( s10, CSTemp);
          DebugN( s11);

          int    iLay,jLayChan;
          float  HVmaxTemp,HVoperTemp,HVexcTemp;
          string HVremTemp, sectNamet,HVremt;
          int    HVmaxt,  HVopert,  conformFLt, CSconformFLt, HVexct;
          float  Imaxt,CSHVmaxt,CSHVopert,CSImaxt;
          
          int iLayKeep = 1;

          dynClear(HVmax);
          dynClear(HVoper);
          dynClear(HVexc);
          dynClear(HVrem);
          dynClear(conformFL);
          dynClear(Imax);
          dynClear(CSHVmax);
          dynClear(CSHVoper);
          dynClear(CSconformFL);
          dynClear(CSImax);
          
          DebugN("ReadfromFile dynlengths after clear:",dynlen(HVmax),dynlen(CSHVmax));
 
          while (feof(fopenreturn)==0)         //    Read from file and set dpe's 
 
          { 
             fscanf(fopenreturn," %d %d %s    %d %d %d %f    %f %f %d %f %d %s ",iLay,jLayChan,sectNamet,  
                     HVmaxt,  HVopert,  conformFLt,  Imaxt,
                   CSHVmaxt,CSHVopert,CSconformFLt,CSImaxt,HVexct,HVremt);
//             fscanf(fopenreturn,"%d %d %f %f %f %s",iLay,jLayChan,HVmaxTemp,HVoperTemp,
//                    HVexcTemp,HVremTemp);
             DebugN("ReadExistingFile READ FROM FILE: Layer ",iLay," Chan ",jLayChan,sectNamet,
                    "  HVmax ",HVmaxt,"  HVoper ",HVopert,conformFLt,Imaxt,
                   CSHVmaxt,CSHVopert,CSconformFLt,CSImaxt,                    HVexct,HVremt);
             
             if (iLay != iLayKeep)   
               {
                dpSet( $dpChamber + ".maxHV.L"      +(string)iLayKeep + ":", HVmax);
                dpSet( $dpChamber + ".operHV.L"     +(string)iLayKeep + ":", HVoper);
                dpSet( $dpChamber + ".ExceptionHV.L"+(string)iLayKeep + ":", HVexc); 
                dpSet( $dpChamber + ".Notes.L"      +(string)iLayKeep + ":", HVrem);

                dpSet( $dpChamber + ".confFL.L"     +(string)iLayKeep + ":", conformFL);
                dpSet( $dpChamber + ".maxI.L"       +(string)iLayKeep + ":", Imax);
                dpSet( $dpChamber + ".CS-maxHV.L"   +(string)iLayKeep + ":", CSHVmax); 
                dpSet( $dpChamber + ".CS-operHV.L"  +(string)iLayKeep + ":", CSHVoper);
                dpSet( $dpChamber + ".CS-confFL.L"  +(string)iLayKeep + ":", CSconformFL);
                dpSet( $dpChamber + ".CS-maxI.L"    +(string)iLayKeep + ":", CSImax);

                DebugN("ReadExistingFile Layer,dynlen: ",iLayKeep,dynlen(HVmax));           
                for (int i=1; i<=dynlen(HVmax);i++)
                  {
                    DebugN("ReadExistingFile: ",iLayKeep,i,  HVmax[i],  HVoper[i],  conformFL[i],  Imax[i],
                                                       CSHVmax[i],CSHVoper[i],CSconformFL[i],CSImax[i],HVexc[i],HVrem[i]);
                  }
                iLayKeep = iLay;

/*              int driftdp;
                bool driftHVexists;
                               
//              string dpLayChan=$dpChamber+".PS-HVchan.L"+LayNum;
//              dpGet(dpLayChan, layerPS_Chan);

                for (int i=1; i<=LminNumRows[(int)sChnum][iLay]; i++)                                 // The following 15 lines are
                  {
                   if (i==LminNumRows[(int)sChnum][iLay] && DRIFTHVEXISTS[(int) sChnum]==TRUE)        //  not necessary. The values are 
                     {
                       driftdp=dynlen(layerPS_Chan); 
                       dpSet(layerPS_Chan[driftdp] + ".settings.v0:_original.._value", HVoper[i]);    // overwritten in Trapezio 
                     }                                                                                //  initialization
                   else
                     {
                       float temp;
                       dpSet(layerPS_Chan[i] + ".settings.v0:_original.._value", HVoper[i]);
                       dpGet(layerPS_Chan[i] + ".settings.v0:_original.._value",temp);
                       DebugN("LOCK Clicked - V_set :", HVoper[i],temp);
                     }
                  }
*/
                dynClear(HVmax);
                dynClear(HVoper);
                dynClear(HVexc);
                dynClear(HVrem);
                dynClear(conformFL);
                dynClear(Imax);
                dynClear(CSHVmax);
                dynClear(CSHVoper);
                dynClear(CSconformFL);
                dynClear(CSImax);

              }

             dynAppend(HVmax,       HVmaxt);
             dynAppend(HVoper,      HVopert);
             dynAppend(conformFL,   conformFLt);
             dynAppend(Imax,        Imaxt);
             dynAppend(CSHVmax,     CSHVmaxt);
             dynAppend(CSHVoper,    CSHVopert);
             dynAppend(CSconformFL, CSconformFLt);
             dynAppend(CSImax,      CSImaxt);
             dynAppend(HVexc,       HVexct);
             dynAppend(HVrem,       HVremt);

            }

          if (feof(fopenreturn)!=0)         // Found EOF
           { 
             int count = dynlen(HVmax);
/*
             dynRemove(HVmax, count);
             dynRemove(HVoper,count);
             dynRemove(HVexc, count);
             dynRemove(HVrem, count);
             dynRemove(conformFL,   count);
             dynRemove(Imax,        count);
             dynRemove(CSHVmax,     count);
             dynRemove(CSHVoper,    count);
             dynRemove(CSconformFL, count);
             dynRemove(CSImax,      count);
*/              
             dpSet( $dpChamber + ".maxHV.L"      +(string)iLay+":", HVmax);
             dpSet( $dpChamber + ".operHV.L"     +(string)iLay+":", HVoper);
             dpSet( $dpChamber + ".ExceptionHV.L"+(string)iLay+":", HVexc); 
             dpSet( $dpChamber + ".Notes.L"      +(string)iLay+":", HVrem);

             dpSet( $dpChamber + ".confFL.L"     +(string)iLay+":", conformFL);
             dpSet( $dpChamber + ".maxI.L"       +(string)iLay+":", Imax);
             dpSet( $dpChamber + ".CS-maxHV.L"   +(string)iLay+":", CSHVmax); 
             dpSet( $dpChamber + ".CS-operHV.L"  +(string)iLay+":", CSHVoper);
             dpSet( $dpChamber + ".CS-confFL.L"  +(string)iLay+":", CSconformFL);
             dpSet( $dpChamber + ".CS-maxI.L"    +(string)iLay+":", CSImax);

             DebugN("ReadExistingFile Layer,dynlen: ",iLay,dynlen(HVmax));           
             for (int i=1; i<=dynlen(HVmax);i++)
               {
                 DebugN("ReadExistingFile EOF: ",iLay,i,  HVmax[i],  HVoper[i],  conformFL[i],  Imax[i],
                                                        CSHVmax[i],CSHVoper[i],CSconformFL[i],CSImax[i],HVexc[i],HVrem[i]);
               }
           }  

          DebugN("ReadfromFile dynlengths at end:",dynlen(HVmax),dynlen(CSHVmax));
 
          fclose(fopenreturn);
          setValue("maxstrHVinfo","text","HV file was found" ); 

//        delay(3);
//        setValue("maxstrHVinfo","text","ENTER NEW HVmax" ); 
  
}

void initTrapezion()
{
  
         dyn_string HVchan,layerPS_Chan, dynNames;
         bool newstate;
         float check;
         string sChnum=substr($dpChamber,14,1);  
         DebugN(" INITIALIZE TRAPEZIO: Chamber ",$dpChamber,"  Layer: ",$Layer);
         CHAMBONOFF[(int) sChnum] == FALSE;        
         string InstNameTrap="Chamber"+sChnum+"-Layer"+$Layer+"-"+$ChType;    

         setValue("setlayerHV","text","0"  ,"visible",TRUE); 
//         setValue("layerinfo" ,"text","OFF","visible",TRUE);        
          
         string dpLayChan=$dpChamber+".PS-HVchan.L"+$Layer;
         dpGet( dpLayChan, layerPS_Chan);
         DebugN("  Initialization Trapezio dpLayChan: ",dpLayChan);

         newstate = FALSE;
         for (int i=1; i<=LminNumRows[(int)sChnum][(int) $Layer]; i++) 
           {
             if (DRIFTHVEXISTS[(int) sChnum]==TRUE && i==LminNumRows[(int)sChnum][(int) $Layer]) 
               i= dynlen(layerPS_Chan);      // Pickup the drift channel 
             dpSet(layerPS_Chan[i] + ".settings.onOff:_original.._value", newstate);
             dpSet(layerPS_Chan[i] + ".settings.v0:_original.._value",0);
             dpGet(layerPS_Chan[i] + ".settings.onOff:_original.._value", newstate);
             dpGet(layerPS_Chan[i] + ".settings.v0:_original.._value",check);

             int rowNum = 0;
  
             dpConnectUserData("Showstatus",InstNameTrap,TRUE, 
                                        layerPS_Chan[i]+".settings.v0",
                                        layerPS_Chan[i]+".actual.vMon",
                                        layerPS_Chan[i]+".settings.i0",
                                        layerPS_Chan[i]+".actual.iMon",
                                        layerPS_Chan[i]+".settings.rUp",
                                        layerPS_Chan[i]+".settings.rDwn",
                                        layerPS_Chan[i]+".settings.onOff",
                                        layerPS_Chan[i]+".actual.Trip");

        dyn_errClass err;        
        err=getLastError();
        if (dynlen(err)>0) {
          DebugN("An error has occured",err);
          throwError(err); }
        else {
//          DebugN("No dpConnect errors"); 
             }
        
     
      // The value to put in there. Put the +1'th dynString value in there :-(

//    DebugTN("EXITING trapezio  Initialization script");         

           }
         
         setValue("","backCol","{65,195,255}"); 
  
}

         void Showstatus(      string InstNameTrap, 
                               string dpe1, float value1, string dpe2, float value2, 
                               string dpe3, float value3, string dpe4, float value4, 
                               string dpe8, float value8, string dpe9, float value9, 
                               string dpe5, bool  value5, string dpe6, bool  value6)
//                               dyn_string dynNames )
  {


         dyn_string dynNames, layerPS_Chan;
         string sectNames=$dpChamber+".CH-HVchan";
         dpGet( sectNames, dynNames);
         string dpLayChan=$dpChamber+".PS-HVchan.L"+$Layer;
         dpGet( dpLayChan, layerPS_Chan);
//         DebugN("INIT TRAP WORK FUNCTION: ",dpe1,dynNames);

         int ii=0;
         for (int i=1; i<=dynlen(dynNames)-1; i++) 
           {
//             DebugN("substr(dpe1) != layerPS_Chan[i] :",substr(dpe1,7,34),layerPS_Chan[i]);
             if (substr(dpe1,7,34) == layerPS_Chan[i]) 
               { ii=i; 
//                 DebugN(" section index: ",ii);
                break; }
           }
         if (substr(dpe1,7,34)==layerPS_Chan[dynlen(layerPS_Chan)]) ii=dynlen(dynNames);

         string sect;
         if ( ii != 0)  sect = dynNames[ii];
         else           sect = "";
//         DebugN(" section : ",sect);         
    
//    GColNeutral  = "white";
//    string GColOff      =  "{65,195,255}";
//    GColOn       =  "green";
//    GColNotSet   =  "yellow";
//    GColTransit  = "{116,200,37}";
//    GColWarnErr  = "{198,12,27}";
//    GColExcErr   = S7_sysMustWentUnq;
//    GColTrip     = S7_sysMustWentUnq;

    string Col_vset  = GColNeutral;
    string backcolor = GColNeutral; 
    string Col_imax  = GColNeutral; 
    string Col_imon  = GColNeutral; 
    string Col_trip  = GColNeutral;
    string Col_onoff = GColNeutral; 
        
    if (value5==TRUE) backcolor = GColOn;
      else backcolor= GColOff;
    if (value5==TRUE &&  value2<=value1-0.2) backcolor=GColTransit;
    if (value5==TRUE &&  value2>=value1-0.2) backcolor=GColOn;

    if (value4 >= PCURR_WARNING) backcolor = GColWarnErr;
    if (value4 >= PCURR_ERROR  ) 
      {
         backcolor = GColExcErr;
         float val;
         dpGet( dpe1, fval  );
         dpSet( dpe1, fval-2);       
         delay(1);
     }

    if (value6==TRUE) backcolor  = GColTrip;

    if (sect != "") 
      {
        setValue(InstNameTrap+".section-"+sect,"backCol",backcolor);
        if (value5==true) setValue(InstNameTrap+".button-"+sect,"backCol",GColOn );
        else              setValue(InstNameTrap+".button-"+sect,"backCol",GColOff );
      }
       
//        DebugN("SHOW STATUS WORK FUNCTION",value1,value2,value3,value4);
  }      
